﻿using UnityEngine;
using System.Collections;
using UnityEditor;

//name of script we are customizing
[CustomEditor(typeof(CaveGen))]

public class CaveEditor : Editor
{
    private string nameToSave = "Cave01";
    private bool saveInactiveObjects = false;

    static CaveGen myCaveGen;

    //create menu
    public class MenuItems
    {
        [MenuItem("LlamaZOO/Create new Cave")]
        private static void NewMenuOption()
        {
            CaveEditor.CreateNewCave();

        }
    }

    public static void CreateNewCave()
    {
        if(myCaveGen)
            myCaveGen.GenCave();
    }

    public override void OnInspectorGUI()
    {
        //show CaveGen public variables
        base.OnInspectorGUI();
        
        //starting customization
        myCaveGen = (CaveGen)target;

        //create "help" message
        EditorGUILayout.HelpBox("You can create new Caves in Editor Mode. You can save prefab, including traps.", 
            MessageType.Info);

        //creating button to new cave
        if (GUILayout.Button("Create new Cave"))
            CreateNewCave();

        GUILayout.Space(10);
        saveInactiveObjects = GUILayout.Toggle(saveInactiveObjects, "Save inactive objects");

        //create text field to get the name to save
        nameToSave = EditorGUILayout.TextField("Save as:", nameToSave);
        if (GUILayout.Button("Save Cave"))
            SaveCave();

    }

    /// <summary>
    /// SaveCave
    /// 
    /// Create prefabs and assets for all objects and traps in the cave
    /// </summary>
    private void SaveCave()
    {
        foreach (Transform item in myCaveGen.transform)
        {
            string pathToSave = "Assets/Prefab/SavedCaves/" + nameToSave + item.name;
            foreach (Transform myChild in item.gameObject.transform)
            {
                GameObject go = myChild.gameObject;

                //Verify if needs to save deactive objects 
                if (! saveInactiveObjects)
                {
                    if(! go.activeInHierarchy)
                        continue;
                }

                Mesh m1 = go.GetComponent<MeshFilter>().sharedMesh;
                if (m1 == null)
                {
                    Debug.LogWarning("Remember to create a new cave before saving with another name.");
                    return;
                }

                //floor uses Default mesh called Plane
                if ( (m1.name != "Plane") && (m1.name != "Sphere"))
                {
                    //create asset if doesn't exist
                    if (AssetDatabase.Contains(m1))
                        Debug.LogWarning(m1.name+":Already exist! Remember to create a new cave before saving with another name.");
                    else
                        AssetDatabase.CreateAsset(m1, pathToSave + myChild.GetHashCode() + "_"+ go.name + "_Mesh" + ".asset");
                }

                PrefabUtility.CreatePrefab(pathToSave + myChild.GetHashCode() + "_" + go.name + ".prefab", go);
                AssetDatabase.Refresh();

            }

        }
    }
}

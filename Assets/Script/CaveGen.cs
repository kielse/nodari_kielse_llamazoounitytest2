﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CaveGen : MonoBehaviour
{
    //Definition of our tiles / states
    public const int CELL_FREE = 0;
    public const int CELL_WALL = 1;
    public const int CELL_TRAP = 2;
    public const int CELL_PLAYER_START_POSITION = 3;

    [Tooltip("Define how much internal free space this cave has")]
    [Range(50, 70)]
    public int wideCave = 50;

    int[,] grid_cells;

    [Tooltip("Number of cells in x dimension")]
    [Range(50, 200)]
    public int max_grid_x = 64;

    [Tooltip("Number of cells in z dimension")]
    [Range(50, 200)]
    public int max_grid_z = 48;

    [Tooltip ("Defice how much space between walls")]
    [Range (1,10)]
    public int SmoothCave=6;

    [Tooltip("Define the size of each cell (square)")]
    [Range(1,5)]
    public float cellSize=1;


    [Tooltip("Define the size of the borders")]
    [Range(1, 10)]
    public int borderSize=5;

    [Tooltip("Define the height of the walls")]
    [Range(1, 100)]
    public float wallHeight = 5;

    [Tooltip("Define the size of little walls to be removed")]
    [Range(1, 200)]
    public int wallThresholdSize=40;

    [Tooltip("Define the size of little rooms to be removed")]
    [Range(1, 200)]
    public int roomThresholdSize=40;

    [Tooltip("Define the size of passage between rooms")]
    [Range(1, 10)]
    public int SizeOfPassageBetweenRooms = 1;


    [Tooltip("Define the percentual of traps in the cave")]
    [Range(0, 1)]
    public float TrapPercentual=0;

    private PoolManager poolManager;

    public GameObject floor;

    public Vector3 playerStartPosition { get; private set; }

    // Use this for initialization
    void Start()
    {
        poolManager = GetComponent<PoolManager>();
        GenCave();
    }


    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// GenCave()
    /// 
    /// Generete caves based on defined parameter. The shape will be randomly
    /// </summary>
    public void GenCave()
    {
        //creating grid 2D
        grid_cells = new int[max_grid_x, max_grid_z];
        
        //Define if a cell is free or wall randomly
        for (int x = 0; x < max_grid_x; x++)
        {
            for (int z = 0; z < max_grid_z; z++)
            {
                if (x == 0 || x == max_grid_x - 1 ||//borders of x
                    z == 0 || z == max_grid_z -1)   //borders of z
                    grid_cells[x, z] = CELL_WALL;
                else
                    grid_cells[x, z] = DefineNextCellState();//is not on a border, so it will be wall or free (randombly)
            }
        }

        //open more free space to walk
        for (int i = 0; i < SmoothCave; i++)
        {
            OpenFreeSpaceAround();
        }

        //Removing small walls
        ImproveCaveShape();


        //creating border with walls
        int[,] borderGrid = new int[max_grid_x + borderSize * 2, max_grid_z + borderSize * 2];
        for (int x = 0; x < borderGrid.GetLength(0); x++)
        {
            for (int z = 0; z < borderGrid.GetLength(1); z++)
            {
                //x is inside of the grid
                if ((x >= borderSize) && x < (max_grid_x + borderSize) &&
                        (z >= borderSize && z < (max_grid_z + borderSize)))
                {
                    borderGrid[x, z] = grid_cells[x - borderSize, z - borderSize];
                }
                else //not inside the grid
                {
                    borderGrid[x, z] = CELL_WALL;
                }
            }
        }

        //creating Mesh
        MeshGen meshGenarator = GetComponent<MeshGen>();
        meshGenarator.GenerateMesh(borderGrid, cellSize, wallHeight);

        ResizeFloor();

        DefineCellsStates();

}

    /// <summary>
    /// ResizeFloor()
    /// 
    /// Update scale and position of the floor
    /// </summary>
    private void ResizeFloor()
    {
        floor.transform.localScale = new Vector3(max_grid_x / 9 *(cellSize), transform.localScale.y, max_grid_z / 9 * (cellSize));
        floor.transform.position = new Vector3(floor.transform.position.x, -wallHeight, floor.transform.position.z);
    }


    /// <summary>
    /// DefineCellsStates()
    /// 
    /// Put traps and other possible states on free cells
    /// </summary>
    void DefineCellsStates()
    {
        //free all existing traps
        if (Application.isPlaying)
            poolManager.FreeAllObjects();

        //go through all free cells and define position for traps
        List<List<Coordinnates>> roomRegions = GetRegions(CELL_FREE);
        foreach (List<Coordinnates> roomRegion in roomRegions)
        {
            //randomly insert the start position for the player
            int indexToPutPlayer = (int)UnityEngine.Random.Range((float)(0f), (float)roomRegion.Count);
            Coordinnates coordPlayer = roomRegion[indexToPutPlayer];
            grid_cells[coordPlayer.tileX, coordPlayer.tileZ] = CELL_PLAYER_START_POSITION;
            playerStartPosition= new Vector3((float)cellSize * ((float)coordPlayer.tileX - ((float)(max_grid_x - 1) / 2f)),
                                -wallHeight,
                                (float)cellSize * ((float)coordPlayer.tileZ - ((float)(max_grid_z - 1) / 2f)));
            roomRegion.RemoveAt(indexToPutPlayer);
            

            float numberOfTraps_i = 0;
            foreach (Coordinnates item in roomRegion)
            {
                //its time to put a trap
                numberOfTraps_i+=TrapPercentual;
                if (numberOfTraps_i >= 1)
                {
                    numberOfTraps_i -= 1;
                    //creating a trap
                    grid_cells[item.tileX, item.tileZ] = CELL_TRAP;
                    if (Application.isPlaying)
                    {
                        GameObject go = poolManager.GetFreeObject();
                        if (go)
                        {
                            go.transform.position = new Vector3((float)cellSize * ((float)item.tileX -( (float)(max_grid_x-1) / 2f)), 
                                -wallHeight,
                                (float)cellSize * ((float)item.tileZ - ((float)(max_grid_z-1) / 2f)));
                            go.SetActive(true);
                        }
                        
                    }
                    
                }
            }
        }
    }


    /// <summary>
    /// DefineNextCellState()
    /// 
    /// Randomly decide if the cell will be free or not
    /// </summary>
    /// <returns></returns>
    int DefineNextCellState()
    {
        if ((UnityEngine.Random.value*100) > wideCave)
            return CELL_WALL;
        else
            return CELL_FREE;
    }


    /// <summary>
    /// OpenFreeSpaceAround()
    /// 
    /// Changes the cell to the most likely of its neighbour. If more free neighour, it will change to be free, and vice-versa.
    /// </summary>
    void OpenFreeSpaceAround()
    {
        for (int x = 0; x < max_grid_x; x++)
        {
            for (int z = 0; z < max_grid_z; z++)
            {
                int neighbourWalls = CountWallNeighbours(x,z);
                if (neighbourWalls >= 5)//lots of walls around
                    grid_cells[x, z] = CELL_WALL;
                else if(neighbourWalls<=3)//lots of free around
                    grid_cells[x, z] = CELL_FREE;
                //if neighbourWall = 4 let it be what was before
            }
        }
    }

    /// <summary>
    /// CountWallNeighbours
    /// 
    /// Search in a 3x3 grid for walls
    /// </summary>
    /// <param name="grid_x">cell x</param>
    /// <param name="grid_z">cell z</param>
    /// <returns>return the count of walls found around the cel grid_x,grid_z</returns>
    int CountWallNeighbours(int grid_x, int grid_z)
    {
        int count = 0;
        //looking arround all his neighbours
        //for every neighbour on x
        for (int neighbour_x = grid_x - 1; neighbour_x <= grid_x + 1; neighbour_x++)
        {
            //for every neighbour on z
            for (int neighbour_z = grid_z - 1; neighbour_z <= grid_z + 1; neighbour_z++)
            {
                //lets see if is not out side of the limits (-1 or greater than the max size on x or z)
                if (neighbour_x >= 0 && neighbour_x < max_grid_x &&
                    neighbour_z >= 0 && neighbour_z < max_grid_z)
                {
                    //lets ignore the center cell grid_x, grid_z
                    if (neighbour_x != grid_x || neighbour_z != grid_z)
                    {
                        //now is really a neighbour, it will increase if is a wall
                        count += grid_cells[neighbour_x, neighbour_z];
                    }
                }
                else
                {
                    //so is out side of the limits, than increase to have more walls on the edges of the map
                    count++;
                }
            }
        }
        return count;
    }


    /// <summary>
    /// Coordinnates
    /// 
    /// To store the tiles (where in the grid)
    /// </summary>
    class Coordinnates
    {
        public int tileX;
        public int tileZ;

        public Coordinnates()
        {
        }

        public Coordinnates(int tile_x, int tile_z)
        {
            this.tileX = tile_x;
            this.tileZ = tile_z;
        }
    }

    /// <summary>
    /// GetRegionTiles
    /// 
    /// Create a list of coordinates 
    /// </summary>
    /// <param name="startx"></param>
    /// <param name="startz"></param>
    /// <returns></returns>
    List<Coordinnates> GetRegionTiles(int startx, int startz)
    {
        List<Coordinnates> tiles = new List<Coordinnates>();
        int[,] gridFlags = new int[max_grid_x, max_grid_z];
        int tileType = grid_cells[startx, startz];

        //puting first item in a queue
        Queue<Coordinnates> queue = new Queue<Coordinnates>();
        queue.Enqueue(new Coordinnates(startx, startz));
        //set the grid as checked
        gridFlags[startx, startz] = 1;

        while (queue.Count > 0)
        {
            Coordinnates tile = queue.Dequeue();
            //add this to the list of tiles
            tiles.Add(tile);
            //look to adjacent tiles
            for (int x = tile.tileX-1; x <= tile.tileX+1; x++)
            {
                for (int z = tile.tileZ-1; z <= tile.tileZ +1; z++)
                {
                    //if the tile is in the grid and diagonal
                    if (IsInGridRange(x, z) && (x==tile.tileX || z == tile.tileZ))
                    {
                        //if we didn't check this yet and is same type
                        if ((gridFlags[x, z] == 0) && (grid_cells[x,z]==tileType))
                        {
                            gridFlags[x, z] = 1;
                            //put the next item in the queue
                            queue.Enqueue(new Coordinnates(x, z));
                        }

                    }
                }
            }
        }

        return tiles;
    }

    bool IsInGridRange(int x, int z)
    {
        return (x >= 0 && x < max_grid_x && z >= 0 && z < max_grid_z);
    }


    /// <summary>
    /// ImproveCaveShape
    /// 
    /// Remove litle walls and little rooms
    /// </summary>
    void ImproveCaveShape() {
        //removing litle wall blocks
        List<List<Coordinnates>> wallRegions = GetRegions(CELL_WALL);
        foreach (List<Coordinnates> wallRegion in wallRegions)
        {
            if (wallRegion.Count< wallThresholdSize)
            {
                //redefine every tile as a free space
                foreach (Coordinnates item in wallRegion)
                {
                    grid_cells[item.tileX, item.tileZ] = CELL_FREE;
                }
            }
        }

        //removing litle rooms
        List<List<Coordinnates>> roomRegions = GetRegions(CELL_FREE);
        List<Room> goodSizerooms = new List<Room>();
        foreach (List<Coordinnates> roomRegion in roomRegions)
        {
            //remove room if is not big enough
            if (roomRegion.Count < roomThresholdSize)
            {
                //redefine every tile as a free space
                foreach (Coordinnates item in roomRegion)
                {
                    grid_cells[item.tileX, item.tileZ] = CELL_WALL;
                }
            }
            else //if is good enough
            {
                goodSizerooms.Add(new Room(roomRegion, grid_cells));
            }
        }

        goodSizerooms.Sort();
        goodSizerooms[0].isMainRoom = true;
        goodSizerooms[0].isAccessbleFromMainroom= true;

        //connect the rooms in the cave
        ConnectRooms(goodSizerooms);

    }

    /// <summary>
    /// ConnectRooms
    /// 
    /// Connect all rooms in the cave
    /// </summary>
    /// <param name="rooms">All rooms in the cave</param>
    /// <param name="forceAccessibFromMainroom">It will force every room to try to find a connection to main room</param>
    void ConnectRooms(List<Room> rooms, bool forceAccessibFromMainroom=false)
    {

        int bestDistance = 0;
        Coordinnates bestTileA = new Coordinnates();
        Coordinnates bestTileB = new Coordinnates();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        bool possibleConnectionFnd = false;

        
        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();
        if (forceAccessibFromMainroom)
        {
            foreach (Room item in rooms)
            {
                //add connected rooms to list b and not connected to list A
                if (item.isAccessbleFromMainroom)
                    roomListB.Add(item);
                else
                    roomListA.Add(item);
            }
        }
        else
        {
            roomListA = rooms;
            roomListB = rooms;
        }

        //search for the closest
        foreach (Room A in roomListA)
        {
            if (!forceAccessibFromMainroom)
            {
                possibleConnectionFnd = false;
                if (A.connectdRooms.Count > 0)
                    continue;//already has a connection
            }

            foreach (Room B in roomListB)
            {
                //ignore if is the same room or is already connected
                if (A == B || A.isConnectdRoom(B))
                    continue;

                

                //looking to all tiles in room A
                for (int tileIndxA = 0; tileIndxA < A.edgeTiles.Count; tileIndxA++)
                {
                    for (int tileIndxB = 0; tileIndxB < B.edgeTiles.Count; tileIndxB++)
                    {
                        Coordinnates tileA = A.edgeTiles[tileIndxA];
                        Coordinnates tileB = B.edgeTiles[tileIndxB];
                        //calculate the distance
                        int distance = (int)(
                            (Mathf.Pow(tileA.tileX - tileB.tileX, 2)) +
                            (Mathf.Pow(tileA.tileZ - tileB.tileZ, 2)));
                        //if found best distance or is the first possible connection
                        if (distance<bestDistance || !possibleConnectionFnd)
                        {
                            bestDistance = distance;
                            possibleConnectionFnd = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = A;
                            bestRoomB = B;
                        }
                    }
                }
            }

            // if we found connection between A and B, create a passage
            if (possibleConnectionFnd && !forceAccessibFromMainroom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (possibleConnectionFnd && forceAccessibFromMainroom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            //call the method again to try to find other connections
            ConnectRooms(rooms, true);
        }
        if (!forceAccessibFromMainroom)
        {
            ConnectRooms(rooms, true);
        }
    }

    /// <summary>
    /// CreatePassage
    /// 
    /// Create a passage between this two rooms
    /// </summary>
    /// <param name="bestRoomA"></param>
    /// <param name="bestRoomB"></param>
    /// <param name="bestTileA"></param>
    /// <param name="bestTileB"></param>
    private void CreatePassage(Room bestRoomA, Room bestRoomB, Coordinnates bestTileA, Coordinnates bestTileB)
    {
        //set connection between both
        Room.ConnectRoom(bestRoomA, bestRoomB);

        List<Coordinnates> line = GetLine(bestTileA, bestTileB);
        foreach (Coordinnates item in line)
        {
            DrawCircle(item, SizeOfPassageBetweenRooms);
        }
        


    }

    /// <summary>
    /// DrawCircle
    /// 
    /// Create a circle (hole) to create passage between rooms
    /// </summary>
    /// <param name="coord"></param>
    /// <param name="radius"></param>
    void DrawCircle(Coordinnates coord, int radius)
    {
        for (int x = -radius; x <= radius; x++)
        {
            for (int z = -radius; z <= radius; z++)
            {
                //if is inside of the circle
                if(x*x + z*z <= radius * radius)
                {
                    //create the point to draw
                    int drawX = coord.tileX + x;
                    int drawZ = coord.tileZ + z;

                    //if is inside of the grid
                    if (IsInGridRange(drawX, drawZ))
                    {
                        grid_cells[drawX, drawZ] = CELL_FREE;
                    }
                }
            }

        }
    }

    /// <summary>
    /// GetLine
    /// 
    /// List of coordinates for each point in the line
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    List<Coordinnates> GetLine(Coordinnates from, Coordinnates to)
    {
        List<Coordinnates> line = new List<Coordinnates>();

        int x = from.tileX;
        int z = from.tileZ;

        int dx = to.tileX - from.tileX;
        int dz = to.tileZ - from.tileZ;

        bool inverted = false;
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dz);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dz);

        if(longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dz);
            shortest = Mathf.Abs(dx);
            step = Math.Sign(dz);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i = 0; i < longest; i++)
        {
            line.Add(new Coordinnates(x, z));
            if (inverted)
                z += step;
            else
                x += step;
            gradientAccumulation += shortest;
            if(gradientAccumulation>= longest)
            {
                if (inverted)
                    x += gradientStep;
                else
                    z += gradientStep;
                gradientAccumulation -= longest;
            }
        }

        return line;
    }

    /// <summary>
    /// GetRegions
    /// 
    /// return all the regions (list of lists) of this type of tile
    /// </summary>
    /// <param name="tileType"></param>
    /// <returns></returns>
    List<List<Coordinnates>> GetRegions(int tileType)
    {
        List<List<Coordinnates>> regions = new List<List<Coordinnates>>();

        int[,] gridFlags = new int[max_grid_x, max_grid_z];

        for (int x = 0; x < max_grid_x; x++)
        {
            for (int z = 0; z < max_grid_z; z++)
            {
                //going to all grid
                if (( gridFlags[x, z] == CELL_FREE) && (grid_cells[x,z] ==tileType))
                {
                    List<Coordinnates> newregion = GetRegionTiles(x, z);
                    regions.Add(newregion);
                    //mark all tiles as looked
                    foreach (Coordinnates item in newregion)
                    {
                        gridFlags[item.tileX, item.tileZ] = 1;
                    }
                }
            }
        }

        return regions;
    }

    /// <summary>
    /// Room
    /// 
    /// All information about rooms in the cave
    /// </summary>
    class Room:IComparable<Room>
    {
        public List<Coordinnates> tiles;
        public List<Coordinnates> edgeTiles;
        public List<Room> connectdRooms;
        public int roomSize;

        public bool isMainRoom;
        public bool isAccessbleFromMainroom;

        public Room() { }
        public Room(List<Coordinnates> roomTiles, int[,] grid)
        {
            this.tiles = roomTiles;
            roomSize = tiles.Count;
            connectdRooms = new List<Room>();

            edgeTiles = new List<Coordinnates>();

            //go through all tiles in this room
            foreach (Coordinnates item in tiles)
            {
                //if is a wall is edge
                for (int x = item.tileX - 1; x <= item.tileX + 1; x++)
                {
                    {
                        for (int z = item.tileZ - 1; z <= item.tileZ + 1; z++)
                        {
                            //if not diagonal neighbour
                            if (x == item.tileX || z == item.tileZ)
                            {
                                if (grid[x, z]==CELL_WALL)
                                {
                                    edgeTiles.Add(item);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// SetAccessbleFromMainRoom
        /// 
        /// Define if the room is accessable from the main room
        /// </summary>
        public void SetAccessbleFromMainRoom()
        {
            if (!isAccessbleFromMainroom)
            {
                isAccessbleFromMainroom = true;
                foreach (Room item in connectdRooms)
                {
                    item.SetAccessbleFromMainRoom();
                }
            }

        }


        /// <summary>
        /// CompareTo
        /// 
        /// Implemented to use IComparable interface
        /// 
        /// </summary>
        /// <param name="otherRoom"></param>
        /// <returns></returns>
        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(this.roomSize);
        }


        /// <summary>
        /// ConnectRoom
        /// 
        /// Connect two rooms
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        public static void ConnectRoom(Room A, Room B)
        {
            if (A.isAccessbleFromMainroom)
                B.SetAccessbleFromMainRoom();
            else if (B.isAccessbleFromMainroom)
                A.SetAccessbleFromMainRoom();

            A.connectdRooms.Add(B);
            B.connectdRooms.Add(A);
        }

        public bool isConnectdRoom(Room other)
        {
            return connectdRooms.Contains(other);
        }

    }



}

﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Rigidbody rigid;
            if (other.name != "Player")
                rigid = other.GetComponentInParent<Rigidbody>();
            else
                rigid = other.GetComponent<Rigidbody>();
            rigid.AddForce(300*Vector3.up);
            this.gameObject.SetActive(false);
        }
    }

}

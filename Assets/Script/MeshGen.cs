﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MeshGen : MonoBehaviour {

    const uint CONFIGURATION_BIT1 = 8;
    const uint CONFIGURATION_BIT2 = 4;
    const uint CONFIGURATION_BIT3 = 2;
    const uint CONFIGURATION_BIT4 = 1;

    const uint POINT_0_SELECTED = 0;

    const uint POINT_1_SELECTED_BOT_LEFT = 1;
    const uint POINT_1_SELECTED_BOT_RIGHT = 2;
    const uint POINT_1_SELECTED_TOP_RIGHT = 4;
    const uint POINT_1_SELECTED_TOP_LEFT = 8;

    const uint POINT_2_SELECTED_BOT_LEFT_BOT_RIGHT = 3;
    const uint POINT_2_SELECTED_BOT_RIGHT_TOP_RIGHT = 6;
    const uint POINT_2_SELECTED_TOP_LEFT_BOT_LEFT = 9;
    const uint POINT_2_SELECTED_TOP_LEFT_TOP_RIGHT = 12;
    const uint POINT_2_SELECTED_TOP_RIGHT_BOT_LEFT = 5;
    const uint POINT_2_SELECTED_TOP_LEFT_BOT_RIGHT = 10;

    const uint POINT_3_SELECTED_TOP_RIGHT_BOT_RIGHT_BOT_LEFT = 7;
    const uint POINT_3_SELECTED_TOP_LEFT_BOT_RIGHT_BOT_LEFT = 11;
    const uint POINT_3_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_LEFT = 13;
    const uint POINT_3_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_RIGHT = 14;

    const uint POINT_4_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_RIGHT_BOT_LEFT = 15;

    public SquareGrid squareGrid;
    List<Vector3> vertices;
    List<int> shapes;

    //using the key "int" it will say all the triangles related to it
    Dictionary<int, List<MyTriangle>> triangleDictionary = new Dictionary<int, List<MyTriangle>>();

    //used to store our outlineEdges (A list of list of index)
    List<List<int>> outlines = new List<List<int>>();

    //for otimization using a hash to don't check the same vertice more than one time
    HashSet<int> checkVertices = new HashSet<int>();

    private float wallHeight;

    public MeshFilter Walls;

    public MeshFilter caveMesh;

    public void GenerateMesh(int[,] grid_cells, float squareSize, float wallH)
    {

        //cleaning
        triangleDictionary.Clear();
        outlines.Clear();
        checkVertices.Clear();
        this.wallHeight = wallH;

        //creating new lists
        vertices = new List<Vector3>();
        shapes = new List<int>();
        squareGrid = new SquareGrid(grid_cells, squareSize);

        //navigating trhough every cell to draw the shape
        for (int x = 0; x < squareGrid.squares.GetLength(0); x++)
        {
            for (int z = 0; z < squareGrid.squares.GetLength(1); z++)
            {
                //create shape for every cell
                DrawCell(squareGrid.squares[x, z]);
            }
        }

        //creating the Mesh for the cave based on all triangles that we have
        Mesh myMesh = new Mesh();
        //attaching the mesh filter
        caveMesh.mesh = myMesh;
        //defining our mesh vertices and triangles
        myMesh.vertices = vertices.ToArray();
        myMesh.triangles = shapes.ToArray();
        //update mesh
        myMesh.RecalculateNormals();

        CreateWallsMesh();

    }


    /// <summary>
    /// CreateWallsMesh
    /// 
    /// create walls based on outlies 
    /// </summary>
    private void CreateWallsMesh()
    {

        CalculateMeshOutlines();

        List<Vector3> wallVertices = new List<Vector3>();
        List<int> wallTriangs = new List<int>();
        Mesh wallmesh = new Mesh();

        foreach (List<int> outline in outlines)
        {
            for (int i = 0; i < outline.Count-1; i++)
            {
                //number of items on wall vertices
                int startIndex = wallVertices.Count;
                //left
                wallVertices.Add(vertices[outline[i]]);
                //right
                wallVertices.Add(vertices[outline[i+1]]);

                //left bott
                wallVertices.Add(vertices[outline[i]]-Vector3.up*wallHeight);
                //right bott
                wallVertices.Add(vertices[outline[i + 1]] - Vector3.up * wallHeight);

                //ading wall triangles going anti clockwise

                //first triangle
                wallTriangs.Add(startIndex + 0);//top lef vertex
                wallTriangs.Add(startIndex + 2);//bot left vertex
                wallTriangs.Add(startIndex + 3);//bot right vertex

                //second triangle
                wallTriangs.Add(startIndex + 3);//bot right vertex
                wallTriangs.Add(startIndex + 1);//top right vertex
                wallTriangs.Add(startIndex + 0);//top left vertex

            }
        }

        wallmesh.vertices = wallVertices.ToArray();
        wallmesh.triangles = wallTriangs.ToArray();

        //assign to mesh filter
        Walls.mesh = wallmesh;

        //adding collider
        MeshCollider wallColider = Walls.gameObject.GetComponent<MeshCollider>();
        if(wallColider==null)
            wallColider = Walls.gameObject.AddComponent<MeshCollider>();
        wallColider.sharedMesh = wallmesh;
    }



    /// <summary>
    /// DrawCell
    /// 
    /// Draw the shape of the cell based on its configuration
    /// </summary>
    /// <param name="square"> Cell to be drawn</param>
    void DrawCell(MySquare square)
    {
        //16 different possible shapes for every cell
        switch (square.cellConfiguration)
        {
            //No points selected, so we have a free cell (no wall, no mesh needed)
            case POINT_0_SELECTED:
                break;

            //1 point selected, it will draw a triangle
            case POINT_1_SELECTED_BOT_LEFT:
                MeshFromPoints(square.centerLeft, square.centerBottom, square.bottomLeft);
                break;
            case POINT_1_SELECTED_BOT_RIGHT:
                MeshFromPoints(square.bottomRight, square.centerBottom, square.centerRight);
                break;
            case POINT_1_SELECTED_TOP_RIGHT:
                MeshFromPoints(square.topRigh, square.centerRight, square.CenterTop);
                break;
            case POINT_1_SELECTED_TOP_LEFT:
                MeshFromPoints(square.topLeft, square.CenterTop, square.centerLeft);
                break;

            //2 points selected, it will draw a rectangle (half of the square) or hexagonon for diagonal points
            case POINT_2_SELECTED_BOT_LEFT_BOT_RIGHT:
                MeshFromPoints(square.centerRight, square.bottomRight, square.bottomLeft,square.centerLeft);
                break;
            case POINT_2_SELECTED_BOT_RIGHT_TOP_RIGHT:
                MeshFromPoints(square.CenterTop, square.topRigh, square.bottomRight, square.centerBottom);
                break;
            case POINT_2_SELECTED_TOP_LEFT_BOT_LEFT:
                MeshFromPoints(square.topLeft, square.CenterTop, square.centerBottom, square.bottomLeft);
                break;
            case POINT_2_SELECTED_TOP_LEFT_TOP_RIGHT:
                MeshFromPoints(square.topLeft, square.topRigh, square.centerRight, square.centerLeft);
                break;
            case POINT_2_SELECTED_TOP_RIGHT_BOT_LEFT:
                MeshFromPoints(square.CenterTop, square.topRigh, square.centerRight, square.centerBottom, square.bottomLeft,square.centerLeft);
                break;
            case POINT_2_SELECTED_TOP_LEFT_BOT_RIGHT:
                MeshFromPoints(square.topLeft, square.CenterTop, square.centerRight, square.bottomRight, square.centerBottom, square.centerLeft);
                break;

            // 3 points selected, it will drawn pentagon shapes
            case POINT_3_SELECTED_TOP_RIGHT_BOT_RIGHT_BOT_LEFT:
                MeshFromPoints(square.CenterTop, square.topRigh, square.bottomRight, square.bottomLeft, square.centerLeft);
                break;
            case POINT_3_SELECTED_TOP_LEFT_BOT_RIGHT_BOT_LEFT:
                MeshFromPoints(square.topLeft, square.CenterTop, square.centerRight, square.bottomRight, square.bottomLeft);
                break;
            case POINT_3_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_LEFT:
                MeshFromPoints(square.topLeft, square.topRigh, square.centerRight, square.centerBottom, square.bottomLeft);
                break;
            case POINT_3_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_RIGHT:
                MeshFromPoints(square.topLeft, square.topRigh, square.bottomRight, square.centerBottom, square.centerLeft);
                break;

            // all 4 points selected, draw full square
            case POINT_4_SELECTED_TOP_LEFT_TOP_RIGHT_BOT_RIGHT_BOT_LEFT:
                MeshFromPoints(square.topLeft, square.topRigh, square.bottomRight, square.bottomLeft);
                //all surround are walls, add to our list of vertices
                checkVertices.Add(square.topLeft.vertexIndex);
                checkVertices.Add(square.topRigh.vertexIndex);
                checkVertices.Add(square.bottomRight.vertexIndex);
                checkVertices.Add(square.bottomLeft.vertexIndex);
                break;

        }
    }


    /// <summary>
    /// MeshFromPoints
    /// 
    /// Create a mesh based on the number of nodes
    /// 
    /// 
    /// </summary>
    /// <param name="points"></param>
    void MeshFromPoints(params MyNode[] points)
    {
        //Assign our points to our list of vertices
        AssignVertices(points);

        if (points.Length>=3)//creating the first part of the image. We have at least one triangle
            CreateShapes(points[0], points[1], points[2]);
        if (points.Length >= 4)//creating the second part of the image. we have two triangles
            CreateShapes(points[0], points[2], points[3]);
        if (points.Length >= 5)//creating the third part of the image. We have three triangles
            CreateShapes(points[0], points[3], points[4]);
        if (points.Length >= 6)//creating the last part of the image. We have four triangles (for cases where diagonnal points are selected: hexagon)
            CreateShapes(points[0], points[4], points[5]);
        
    }


    /// <summary>
    /// AssignVertices()
    /// Assign our points to our list of positions (vertices)
    /// </summary>
    /// <param name="points"></param>
    void AssignVertices(MyNode[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            //if we this point hasn't been assigned
            if (points[i].vertexIndex == MyNode.VERTICE_UNUSED)
            {
                //set this node how many itens it is connected to. 
                points[i].vertexIndex = vertices.Count;
                vertices.Add(points[i].position);
            }
        }
    }

    /// <summary>
    /// CreateShapes()
    /// Based on tree points create a triangle to be used to create the Mesh
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    void CreateShapes(MyNode a, MyNode b, MyNode c)
    {
        //for every triangle we add its 3 points
        shapes.Add(a.vertexIndex);
        shapes.Add(b.vertexIndex);
        shapes.Add(c.vertexIndex);

        MyTriangle triangle = new MyTriangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);

        //Add this triangle for his three index
        AddTriangleToDictionary(triangle.vertexIndexA, triangle);
        AddTriangleToDictionary(triangle.vertexIndexB, triangle);
        AddTriangleToDictionary(triangle.vertexIndexC, triangle);
    }


    /// <summary>
    /// AddTriangleToDictionary
    /// 
    /// Add the triangle to the dictionay set by the key 
    /// </summary>
    /// <param name="vertexIndexKey">Vertex index which the triangles belongs</param>
    /// <param name="triangle"></param>
    void AddTriangleToDictionary(int vertexIndexKey, MyTriangle triangle)
    {
        if (triangleDictionary.ContainsKey(vertexIndexKey))
        {
            //list of triangles for this vertice
            triangleDictionary[vertexIndexKey].Add(triangle);
        }
        else
        {
            //create new list of triangles
            List<MyTriangle> triangleList = new List<MyTriangle>();
            triangleList.Add(triangle);
            triangleDictionary.Add(vertexIndexKey, triangleList);
        }
    }

    /// <summary>
    /// CalculateMeshOutlines
    /// 
    /// Goes through every vertex and check if is an outline until find it self again
    /// </summary>
    void CalculateMeshOutlines()
    {
        for (int vertexIndx = 0; vertexIndx < vertices.Count; vertexIndx++)
        {
            if (!checkVertices.Contains(vertexIndx))
            {
                int newOutlineVertex = GetConnectdOutlineVertex(vertexIndx);
                if (newOutlineVertex != -1)
                {
                    checkVertices.Add(vertexIndx);

                    //create a new list for our outline
                    List<int> newOutline = new List<int>();
                    newOutline.Add(vertexIndx);

                    //adding our new list to our outlines
                    outlines.Add(newOutline);
                    GoThroughOutline(newOutlineVertex, outlines.Count-1);
                    outlines[outlines.Count - 1].Add(vertexIndx);
                }
            }
        }

    }

    /// <summary>
    /// GoThroughOutline
    /// 
    /// Go through the outline recursively adding all the vertex
    /// </summary>
    /// <param name="newOutlineVertex"></param>
    /// <param name="outlineIndex"></param>
    private void GoThroughOutline(int newOutlineVertex, int outlineIndex)
    {
        outlines[outlineIndex].Add(newOutlineVertex);
        checkVertices.Add(newOutlineVertex);

        int nextVertexIndex = GetConnectdOutlineVertex(newOutlineVertex);
        if (nextVertexIndex != -1)
        {
            GoThroughOutline(nextVertexIndex, outlineIndex);
        }

    }


    /// <summary>
    /// IsOutlineEdge
    /// 
    /// if vertexIndexA and B share only one triangle, means that is an outline edge
    /// Given these vertex determine if is an outline edge
    /// </summary>
    /// <param name="vertexIndexA"></param>
    /// <param name="vertexIndex"></param>
    /// <param name=""></param>
    /// <returns>True if there is only one shared triangle</returns>
    bool IsOutlineEdge(int vertexIndexA, int vertexIndexB)
    {
        List<MyTriangle> trianglesVertexA = triangleDictionary[vertexIndexA];
        int count = 0;
        foreach (MyTriangle triangA in trianglesVertexA)
        {
            if (triangA.Contains(vertexIndexB))
            {
                count++;
                if (count > 1)
                    break;
            }
        }
        return count == 1;
    }

    /// <summary>
    /// GetConnectdOutlineVertex
    /// 
    /// Find coneected vertex which form the outline edge
    /// </summary>
    /// <param name="vertexIndex"></param>
    /// <returns></returns>
    int GetConnectdOutlineVertex( int vertexIndex)
    {
        List<MyTriangle> triangleContaVertexA = triangleDictionary[vertexIndex];
        foreach (MyTriangle triangle in triangleContaVertexA)
        {
            for (int i = 0; i < 3; i++)//there are three vertex on our shape (triangle)
            {
                int vertexB = triangle[i];

                //if the vertexindex are different and not ckecked yet
                if ((vertexB != vertexIndex) &&(!checkVertices.Contains(vertexB)))
                {
                    if (IsOutlineEdge(vertexIndex, vertexB))
                    {
                        return vertexB;
                    }
                }
            }
        }

        //if didn't find
        return -1;
    }

    //Creating auxiliar class

    /// <summary>
    /// MyTriangle
    /// 
    /// Used to determine wich triangles every vertices belongs to
    /// </summary>
    public class MyTriangle
    {
        public int vertexIndexA;
        public int vertexIndexB;
        public int vertexIndexC;

        int[] vertices;

        public MyTriangle(int a, int b, int c)
        {
            //There are 3 vertices A, B or C
            vertices = new int[3];

            vertices[0] = this.vertexIndexA = a;
            vertices[1] = this.vertexIndexB = b;
            vertices[2] = this.vertexIndexC = c;

        }

        //creating an index to access the triangle like an array[]
        public int this[int i]
        {
            get
            {
                return vertices[i];
            }
        }

        /// <summary>
        /// Contains
        /// 
        /// Verify if this triangle contains this vertex
        /// </summary>
        /// <param name="vertexIndex"></param>
        /// <returns></returns>
        public bool Contains(int vertexIndex)
        {
            return
                ((vertexIndex == vertexIndexA) ||
                (vertexIndex == vertexIndexB) ||
                    (vertexIndex == vertexIndexC));
        }

    }

    /// <summary>
    /// MyNode
    /// 
    /// This class build every conection between cells
    /// </summary>
    public class MyNode
    {
        public const int VERTICE_UNUSED=-1;
        public Vector3 position;
        public int vertexIndex = VERTICE_UNUSED;

        public MyNode(Vector3 pos)
        {
            this.position = pos;

        }
    }

    /// <summary>
    /// ControlNode
    /// 
    /// This class is used to control the four points around the Square ( our cell )
    /// </summary>
    public class ControlNode: MyNode
    {
        public bool isActive;
        public MyNode above, right;

        public ControlNode (Vector3 pos, bool active, float squareSize) : base(pos)
        {
            this.isActive = active;
            this.above = new MyNode(position + Vector3.forward * squareSize / 2f);
            this.right = new MyNode(position + Vector3.right * squareSize / 2f);
        }
    }

    /// <summary>
    /// MySquare
    /// 
    /// This class define a cell in shape of a square limited by his Nodes. There are 16 possible configuration.
    /// </summary>
    public class MySquare
    {
        public ControlNode topLeft, topRigh, bottomRight, bottomLeft;
        public MyNode CenterTop, centerRight, centerBottom, centerLeft;

        //every cell has 16 possibilities to be drawn. See the documentation for more details
        public uint cellConfiguration;

        public MySquare(ControlNode topL, ControlNode topR, ControlNode botR, ControlNode botL)
        {
            this.topLeft = topL;
            this.topRigh = topR;
            this.bottomLeft = botL;
            this.bottomRight = botR;

            this.CenterTop = this.topLeft.right;
            this.centerRight = this.bottomRight.above;
            this.centerBottom = this.bottomLeft.right;
            this.centerLeft = this.bottomLeft.above;

            //determine the configuration of this cell
            if (topLeft.isActive) cellConfiguration += CONFIGURATION_BIT1;
            if (topRigh.isActive) cellConfiguration += CONFIGURATION_BIT2;
            if (bottomRight.isActive) cellConfiguration += CONFIGURATION_BIT3;
            if (bottomLeft.isActive) cellConfiguration += CONFIGURATION_BIT4;
            

        }
    }

    /// <summary>
    /// SquareGrid
    /// 
    /// This grid is the area of the cave and have all squares (cells) to draw the cave
    /// </summary>
    public class SquareGrid
    {
        public MySquare[,] squares;
        public SquareGrid (int[,] grid_cells, float squareSize)
        {
            int nodeCountX = grid_cells.GetLength(0);
            int nodeCountZ = grid_cells.GetLength(1);
            float gridMaxX = nodeCountX * squareSize;
            float gridMaxZ = nodeCountZ * squareSize;

            ControlNode[,] controlNodes = new ControlNode[nodeCountX, nodeCountZ];

            for (int x = 0; x < nodeCountX; x++)
            {
                for (int z = 0; z < nodeCountZ; z++)
                {
                    Vector3 pos = new Vector3(-gridMaxX / 2 + x * squareSize + squareSize / 2,
                        0,
                        -gridMaxZ / 2 + z * squareSize + squareSize / 2);
                    controlNodes[x, z] = new ControlNode(pos, grid_cells[x, z] == CaveGen.CELL_WALL, squareSize);
                }
            }

            //there is one square lass than nodeCount
            squares = new MySquare[nodeCountX - 1, nodeCountZ - 1];
            for (int x = 0; x < nodeCountX-1; x++)
            {
                for (int z = 0; z < nodeCountZ-1; z++)
                {
                    squares[x, z] = new MySquare(controlNodes[x, z + 1],//left and above
                        controlNodes[x + 1, z + 1],//right and above
                        controlNodes[x + 1, z],//bottom and right
                        controlNodes[x, z]);//bottom and left
                }
            }
        }
    }


}

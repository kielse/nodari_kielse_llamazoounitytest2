﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// PoolManager
/// 
/// This script can be used in any game object to create and control pool of objects.
/// 1. Drag and drop this script to the Inspector to add to a GameObject
/// 2. In the Inspector define the initial number of objects
/// 3. Drag and drop the prefab of the object to be controled (pooling)
/// 4. In your gamecontroler script create a instance of PoolManager and call GetFreeObject() when need a new object
/// 
/// </summary>
public class PoolManager : MonoBehaviour {

    [Tooltip("Drag and drop the prefab of the object to be controled")]
    public GameObject MyObject;

    [Tooltip("Define the parent of the objects")]
    public GameObject ObjectsParent;

    [Tooltip("Initial number of objects")]
    public int poolAmountObjects = 100;

    private List<GameObject> objects;

    void Awake () {
        //Creating initial list of objects
        //Is better do it in Awake than Start, otherwise we can have a calling to GetFreeObject before Start finish
        objects = new List<GameObject>();
        for (int i = 0; i < poolAmountObjects; i++)
        {
            CreateNewObject();
        }
	}

    /// <summary>
    /// CreateNewObject()
    /// 
    /// create new object and deactive
    /// </summary>
    /// <returns></returns>
    private GameObject CreateNewObject()
    {
        GameObject go = (GameObject)Instantiate(MyObject);
        if (ObjectsParent)
            go.transform.SetParent(ObjectsParent.transform);
        objects.Add(go);
        go.SetActive(false);
        return go;
    }


    /// <summary>
    /// GetFreeObject
    /// 
    /// This method can be called to receive a free game object
    /// </summary>
    /// <returns> Return a game object that is free</returns>
    public GameObject GetFreeObject () {
        //search for a free game object in the list
        foreach (GameObject item in objects)
        {
            if (!item.activeInHierarchy)
            {
                //item.SetActive(true);
                return item;
            }
        }
        //if the system needs more objects it will create, but won't destroy
        return CreateNewObject();
    }

    /// <summary>
    /// FreeAllObjects()
    /// 
    /// Free all objects to be reused later
    /// </summary>
    public void FreeAllObjects()
    {
        foreach (GameObject item in objects)
        {
            if (item.activeInHierarchy)
                item.SetActive(false);
        }
    }
}

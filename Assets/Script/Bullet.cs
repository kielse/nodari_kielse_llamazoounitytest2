﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Player")
        {
            if (other.gameObject.tag == "Enemy")
                other.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }

    public float mySpeed = 10;

    void Update()
    {
        transform.Translate(0, 0, mySpeed * Time.deltaTime);
    }
}

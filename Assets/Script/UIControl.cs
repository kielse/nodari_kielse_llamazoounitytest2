﻿using UnityEngine;
using System.Collections;
using System;

public class UIControl : MonoBehaviour {
    private GameObject TopCamera;
    private CaveGen caveGen;
    private bool isTopCameraStartSizeSet;
    private float TopCameraStartSize;


    // Use this for initialization
    void Start () {
        TopCamera = GameObject.Find("TopCamera");
        if (!TopCamera)
            Debug.LogWarning("TopCamera not found!");
        caveGen = FindObjectOfType<CaveGen>();
        if (!caveGen)
            Debug.LogWarning("No CaveGen object was found!");

        isTopCameraStartSizeSet = false;
        TopCameraStartSize = TopCamera.GetComponent<Camera>().orthographicSize;
    }

    private void ResizeTopCamera()
    {
        TopCamera.GetComponent<Camera>().orthographicSize = TopCameraStartSize;
    }

    private float GetSizeCamera()
    {
        if (caveGen)
            return (caveGen.cellSize * (caveGen.borderSize + (caveGen.max_grid_x > caveGen.max_grid_z ? caveGen.max_grid_x : caveGen.max_grid_z))) / 2f;
        else
            return TopCameraStartSize;
    }

    // Update is called once per frame
    void Update () {
        
        if ((!isTopCameraStartSizeSet) || (TopCameraStartSize != GetSizeCamera()))
        {
            isTopCameraStartSizeSet = true;
            TopCameraStartSize = GetSizeCamera();
            ResizeTopCamera();
        }

    }
}
